# Marathono

Marathono is a small GUI tool that helps you manage long-running processes in macOS.

Marathono is not open source. This repository is created for issue tracking purpose only.

[Home Page] (http://www.marathono.com/)

[Twitter] (https://twitter.com/marathono)

[FAQ] (https://gitlab.com/marathono/marathono/blob/master/FAQ.md)

[Report Bugs or Propose New Features] (https://gitlab.com/marathono/marathono/issues)