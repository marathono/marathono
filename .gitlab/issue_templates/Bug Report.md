### Description

Describe the bug here. Attach screenshots and/or crash logs if necessary. Please avoid creating new issues for bugs that have been reported by other users. You could find existing bugs at https://gitlab.com/marathono/marathono/issues?label_name=bug

### macOS version

Full macOS version, e.g., 10.12.1. You could find your macOS version from the Apple menu () -> About This Mac.

### Marathono version

You can find the version from the title bar of Marathono's Preferences window.
