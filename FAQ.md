## FAQ

### My command works in Terminal but doesn't work in Marathono.
While most commands should just work in Marathono the same way as it works in Terminal, Marathono doesn't have the exact same environment as your shell has. You should be able to figure out a workaround very easily by looking at the output of the command (click "Show Output" in the dropdown menu).

### My process doesn't quit.
Some processes may trap SIGINT and/or SIGTERM signal and refuse to quit. If that's case for your process, you may want to select a different kill signal for this process in the "Edit Process" window. SIGKILL cannot be trapped and should always work. If even SIGKILL doesn't kill your process, please file a bug report.

### Can I run short-lived processes with Marathono?
Of course. Nothing stops you from running short-lived processes with Marathono. I do that all the time. I would just imagine people would use Marathono for long-running processes mostly. Oh, by the way, "restart process on file changes" also works for short-lived processes.

### How do I change the order of processes in the list?
Just drag them around.

### How Marathono works under the hood?
Marathono uses [Launchd](https://en.wikipedia.org/wiki/Launchd) to manage life cycles of processes. When a process is launched for the first time, Marathono creates a plist file under `~/Library/LaunchAgents`. When Marathono quits, all plist files managed by Marathono are removed. Marathono runs your commands in a login, interactive pseudo-terminal.

### How do I report bugs or propose new features?
Use [GitLab issue tracker](https://gitlab.com/marathono/marathono/issues). If you don't have a GitLab account, you could sign in with your GitHub in just two mouse clicks. You could also [ask me quick questions on Twitter](https://twitter.com/marathono).

### Why Marathono is not in Mac App Store?
Apple requires all apps deployed to App Store to be sandboxed. However, Marathono allows you to run arbitrary commands and thus it cannot really be sandboxed.
